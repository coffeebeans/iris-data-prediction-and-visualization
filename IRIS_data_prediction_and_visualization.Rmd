---
title: "Wstęp do sztucznej inteligencji - Machine Learning - Notebook 1"
author: "Coffeebeans"
date: "11 kwietnia 2020"
output: html_document
---


```{r , out.width='20%', fig.align='center' }
knitr::include_graphics('hex-rmarkdown.png')
```


### Wgrywanie danych

Załaczam do bazy dane IRIS jednocześnie nazywając je DATASET
```{r}
data(iris)
dataset <- iris
```

Wprowadzam nagłówki kolumn dla zbioru danych DATASET
```{r}
colnames(dataset) <- c("Wysokosc_kielicha","Szerokosc_kielicha","Wysokosc_platka","Szerokosc_platka","Gatunki")
```



### Podział danych na dwa podzbiory

```{r include = FALSE}
library(caret)
```

Tworze liste składającą się z 80% wierszy pierwotnego zbioru DATASET na poczeet trainingu
```{r}
set.seed(1000)
validation_index <- createDataPartition(dataset$Gatunki , p=0.80, list=FALSE)
```

Pozostałe 20% wierszy przeznaczymy na walidacje danych (validation)
```{r}
validation <- dataset[-validation_index,]
```

Ponownie określam zbiór DATASET wcześniej wybranymi 80% danych. Dane te będą następnie szkolone oraz stosowane do testu wybranych algorytmów
```{r}
dataset <- dataset[validation_index,]
```




### Podsumowanie zbioru danych DATASET

Wymiar danych
```{r}
dim(dataset)
```

Sprawdzamy typ danych w poszczególnych kolumnach
```{r}
sapply(dataset, class)
```

Wyświetlam pierwsze 5 wierszy danych
```{r}
head(dataset)
```

Wyświetlam liste wszystkich gatunków Irysów znajdujących sie w kolumnie Gatunki w danych DATASET
```{r}
levels(dataset$Gatunki )
```

 Podsumowanie gatunków Irysów i ich rozkład (liczbowy - liczba wierszy i procentowy). Rozkład na klasy Irysów.
```{r}
percentage <- prop.table(table(dataset$Gatunki )) * 100
cbind(freq=table(dataset$Gatunki ), percentage=percentage)
```

 Podsumowanie każdej z kolumn DATASET (dane statystyczne)
```{r}
summary(dataset)
```




### Wizualizacja danych DATASET
 
 Podział na wartości i argumenty
```{r}
x <- dataset[,1:4]
y <- dataset[,5]
```

 Boxplot - wykres pudełkowy dla każdego atrybutu (argumentow x-ów - pierwsze 4 kolumny) ze zbioru danych IRIS
```{r}
par(mfrow=c(1,4))
  for(i in 1:4) {
  boxplot(x[,i], main=names(iris)[i])
}
```

 Barplot - wykres (histogram) wartości y (kolumna Gatunki). Widzimy, że wyjątkowo rozkład gatunków jest jednakowy i tak jak już wcześniej widzieliśmy liczy po 40 elementów każdy gatunek.
```{r}
plot(y)
```

 Wykres współny pokazujący zależności pomiędzy atrybutami
```{r}
featurePlot(x=x, y=y, plot="ellipse")
```
Niebieskie - virginica
Różowe - versicolor
Nielone - setosa

 Box & Whisker wykresy pokazujące rozkład każdego typu Irysu dla każdego atrybutu osobno
```{r}
featurePlot(x=x, y=y, plot="box")
```

 Density - wykresy dla każdego typu Irysu z podziałem na atrybuty 
```{r}
scales <- list(x=list(relation="free"), y=list(relation="free"))
featurePlot(x=x, y=y, plot="density", scales=scales)
```
Niebieskie - virginica
Różowe - versicolor
Nielone - setosa


### Algorytmy

Każdy algorytm puszczamy 10 razy. Dane podzielamy na 10 części, z czego 9 będzie uczonych, a 1 przeznaczony do testu(oceny).
```{r}
control <- trainControl(method="cv", number=10)
metric <- "Accuracy"
```

 1. Algorytm liniowy - Linear Discriminant Analysis (LDA)
    
```{r}
set.seed(7)
fit.lda <- train(Gatunki~., data=dataset, method="lda", metric=metric, trControl=control)
```

 2. Algorytm nieliniowy - Classification and Regression Trees (CART)
    
```{r}
set.seed(7)
fit.cart <- train(Gatunki~., data=dataset, method="rpart", metric=metric, trControl=control)
```

 3. Zaawansowane algorytmy nieliniowe
  a) Support Vector Machines (SVM) with a linear kernel
  
```{r}
set.seed(7)
fit.svm <- train(Gatunki~., data=dataset, method="svmRadial", metric=metric, trControl=control)
```
  
  b) Random Forest (RF)
    
```{r}
set.seed(7)
fit.rf <- train(Gatunki~., data=dataset, method="rf", metric=metric, trControl=control)
```



### Wybór najlepszego modelu

Podsumowanie dokładności wybranych algorytmów

Kappa wskaźnik - wskazuje na doskonałą zgodność między przewidywaniami modelu a rzeczywistymi wartościami

```{r}
results <- resamples(list(lda=fit.lda, cart=fit.cart, svm=fit.svm, rf=fit.rf))
summary(results)
```

 Wizualizacyjne porównanie algorytmów. Widzimy, że model LDA jest modelem optymalnym
```{r}
dotplot(results)
```




### Podsumowanie najlepszego modelu

Dokładność: 97.5%
```{r}
print(fit.lda)
```




### Przewidywane zachowanie modelu

Oszacowanie umiejętności algorytmu liniowego LDA w zakresie zbioru danych dotyczących walidacji (wykluczone ze szkolenia 20% danych pierwotnych).

Dokładność przewidywań dla próbki 20% danych pierwotnych: 96.67% oraz Kappa 95% sugeruje nam, że mamy dokładny i wiarygodny model.
```{r}
predictions <- predict(fit.lda, validation)
confusionMatrix(predictions, validation$Gatunki)
```
